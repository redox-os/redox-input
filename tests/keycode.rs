use core::convert::TryFrom;
use redox_input::{Keycode, RegularKeycode};

#[test]
fn try_from_str() {
    for c in u16::MIN..=u16::MAX {
        let decimal = format!("{}", c);
        assert!(
            Keycode::try_from(decimal.as_str()).is_ok(),
            "{} can be parsed as ButtonCode",
            decimal
        );

        let hexadecimal = format!("{:x}", c);
        assert!(
            Keycode::try_from(hexadecimal.as_str()).is_ok(),
            "{} can be parsed as ButtonCode",
            hexadecimal
        );

        let hexadecimal = format!("{:X}", c);
        assert!(
            Keycode::try_from(hexadecimal.as_str()).is_ok(),
            "{} can be parsed as ButtonCode",
            hexadecimal
        );
    }
}

#[test]
fn from_u16() {
    for c in u16::MIN..=u16::MAX {
        let keycode = Keycode::from(c);

        match RegularKeycode::is_reserved(c) {
            false => assert!(keycode.is_regular(), "{:?} is a regular keycode", keycode),
            true => assert!(
                !keycode.is_regular(),
                "{:?} is not a regular keycode",
                keycode
            ),
        }

        match RegularKeycode::is_reserved(c) {
            false => {
                assert!(
                    keycode.to_regular().is_some(),
                    "{:?} is a regular keycode",
                    keycode
                );
                assert!(
                    RegularKeycode::try_from(c).is_ok(),
                    "{:?} is a regular keycode",
                    keycode
                );
            }
            true => {
                assert!(
                    keycode.to_regular().is_none(),
                    "{:?} is not regular keycode",
                    keycode
                );
                assert!(
                    RegularKeycode::try_from(c).is_err(),
                    "{:?} is not regular keycode",
                    keycode
                );
            }
        }
    }
}

#[test]
fn into_u16() {
    for c in u16::MIN..=u16::MAX {
        let code = Keycode::from(c);
        assert_eq!(c, u16::from(code), "{:?} == {}", code, c);
    }
}
