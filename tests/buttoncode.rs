use core::convert::TryFrom;
use core::num::NonZeroU16;
use redox_input::ButtonCode;

#[test]
fn try_from_str() {
    for c in u16::MIN..=u16::MAX {
        let decimal = format!("{}", c);
        assert!(
            ButtonCode::try_from(decimal.as_str()).is_ok(),
            "{} can be parsed as ButtonCode",
            decimal
        );

        let hexadecimal = format!("{:x}", c);
        assert!(
            ButtonCode::try_from(hexadecimal.as_str()).is_ok(),
            "{} can be parsed as ButtonCode",
            hexadecimal
        );

        let hexadecimal = format!("{:X}", c);
        assert!(
            ButtonCode::try_from(hexadecimal.as_str()).is_ok(),
            "{} can be parsed as ButtonCode",
            hexadecimal
        );
    }
}

#[test]
fn from_u16() {
    for c in u16::MIN..=u16::MAX {
        let target = ButtonCode::from(c);

        match NonZeroU16::new(c) {
            None => assert_eq!(ButtonCode::None, target, "0 => ButtonCode::None"),
            Some(code) => assert_eq!(ButtonCode::Code(code), target, "{} => {:?}", c, target),
        }
    }
}

#[test]
fn into_u16() {
    assert_eq!(0u16, u16::from(ButtonCode::None), "0 == ButtonCode::None");

    for c in u16::MIN..=u16::MAX {
        let code = ButtonCode::from(c);
        assert_eq!(c, u16::from(code), "{:?} == {}", code, c);
    }
}

#[test]
fn is_primary() {
    for c in u16::MIN..=u16::MAX {
        let code = ButtonCode::from(c);
        match c {
            1 => assert!(code.is_primary(), "{:?} is primary button", code),
            _ => assert!(!code.is_primary(), "{:?} is not primary button", code),
        }
    }
}

#[test]
fn is_secondary() {
    for c in u16::MIN..=u16::MAX {
        let code = ButtonCode::from(c);
        match c {
            2 => assert!(code.is_secondary(), "{:?} is secondary button", code),
            _ => assert!(!code.is_secondary(), "{:?} is not secondary button", code),
        }
    }
}

#[test]
fn is_tertiary() {
    for c in u16::MIN..=u16::MAX {
        let code = ButtonCode::from(c);
        match c {
            3 => assert!(code.is_tertiary(), "{:?} is tertiary button", code),
            _ => assert!(!code.is_tertiary(), "{:?} is not tertiary button", code),
        }
    }
}
