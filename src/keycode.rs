use core::convert::TryFrom;
use core::fmt::{self, Debug, Display, Formatter};
use core::mem;

/// USB HID keycode constants.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum Keycode {
    Regular(RegularKeycode),
    ReservedOrCustom(u16),
}

impl Keycode {
    /// Returns whether this key is a modifier.
    #[inline]
    pub fn is_modifier(&self) -> bool {
        match self {
            Keycode::Regular(r) => r.is_modifier(),
            _ => false,
        }
    }

    #[inline]
    pub fn is_regular(&self) -> bool {
        match self {
            Keycode::Regular(_) => true,
            Keycode::ReservedOrCustom(_) => false,
        }
    }

    #[inline]
    pub fn to_regular(self) -> Option<RegularKeycode> {
        match self {
            Keycode::Regular(r) => Some(r),
            Keycode::ReservedOrCustom(_) => None,
        }
    }
}

impl From<u16> for Keycode {
    #[inline]
    fn from(code: u16) -> Self {
        match RegularKeycode::try_from(code) {
            Err(_) => Self::ReservedOrCustom(code),
            Ok(c) => Self::Regular(c),
        }
    }
}

impl From<Keycode> for u16 {
    #[inline]
    fn from(keycode: Keycode) -> Self {
        match keycode {
            Keycode::Regular(kc) => u16::from(kc),
            Keycode::ReservedOrCustom(custom) => custom,
        }
    }
}

impl From<RegularKeycode> for Keycode {
    #[inline]
    fn from(code: RegularKeycode) -> Self {
        Self::Regular(code)
    }
}

/// USB HID keycode constants.
///
/// # Summary
/// This list is modeled after the keycodes defined in
/// <https://usb.org/sites/default/files/hut1_22.pdf#page=83>.
///
/// The keycodes are the names for **key positions** on a US keyboard.
///
/// # Usage types
/// The usage type of all key codes is **Selectors**, except for the modifier keys which are
/// **Dynamic Flags**:
/// - Left/Right Control
/// - Left/Right Shift
/// - Left/Right Alt
/// - Left/Right GUI
///
/// # Notes
/// Some keys have additional notes like **(1)** up until **(34)**. Please refer to the website in
/// <https://usb.org/sites/default/files/hut1_22.pdf#page=89>.
#[rustfmt::skip]
#[repr(u16)]
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum RegularKeycode {
    /* 0x00 reserved */

    /// **(1)** Keyboard ErrorRollOver
    ErrorRollOver  = 0x01,
    /// **(1)** Keyboard POSTFail
    POSTFail       = 0x02,
    /// **(1)** Keyboard ErrorUndefined
    ErrorUndefined = 0x03,

    // Alphabet
    /// **(2)** Keyboard a and A
    KeyA = 0x04,
    /// Keyboard b and B
    KeyB = 0x05,
    /// **(2)** Keyboard c and C
    KeyC = 0x06,
    /// Keyboard d and D
    KeyD = 0x07,
    /// Keyboard e and E
    KeyE = 0x08,
    /// Keyboard f and F
    KeyF = 0x09,
    /// Keyboard g and G
    KeyG = 0x0A,
    /// Keyboard h and H
    KeyH = 0x0B,
    /// Keyboard i and I
    KeyI = 0x0C,
    /// Keyboard j and J
    KeyJ = 0x0D,
    /// Keyboard k and K
    KeyK = 0x0E,
    /// Keyboard l and L
    KeyL = 0x0F,
    /// **(2)** Keyboard m and M
    KeyM = 0x10,
    /// Keyboard n and N
    KeyN = 0x11,
    /// **(2)** Keyboard o and O
    KeyO = 0x12,
    /// **(2)** Keyboard p and P
    KeyP = 0x13,
    /// **(2)** Keyboard q and Q
    KeyQ = 0x14,
    /// Keyboard r and R
    KeyR = 0x15,
    /// Keyboard s and S
    KeyS = 0x16,
    /// Keyboard t and T
    KeyT = 0x17,
    /// Keyboard u and U
    KeyU = 0x18,
    /// Keyboard v and V
    KeyV = 0x19,
    /// **(2)** Keyboard w and W
    KeyW = 0x1A,
    /// **(2)** Keyboard x and X
    KeyX = 0x1B,
    /// **(2)** Keyboard y and Y
    KeyY = 0x1C,
    /// **(2)** Keyboard z and Z
    KeyZ = 0x1D,

    // Numbers
    /// **(2)** Keyboard 1 and !
    Key1 = 0x1E,
    /// **(2)** Keyboard 2 and @
    Key2 = 0x1F,
    /// **(2)** Keyboard 3 and #
    Key3 = 0x20,
    /// **(2)** Keyboard 4 and $
    Key4 = 0x21,
    /// **(2)** Keyboard 5 and %
    Key5 = 0x22,
    /// **(2)** Keyboard 6 and ^
    Key6 = 0x23,
    /// **(2)** Keyboard 7 and &
    Key7 = 0x24,
    /// **(2)** Keyboard 8 and *
    Key8 = 0x25,
    /// **(2)** Keyboard 9 and (
    Key9 = 0x26,
    /// **(2)** Keyboard 0 and )
    Key0 = 0x27,

    // Misc.
    /// **(3)** Keyboard Return (ENTER)
    KeyReturnEnter  = 0x28,
    /// Keyboard ESCAPE
    KeyEsc          = 0x29,
    /// **(4)** Keyboard DELETE (Backspace)
    KeyDel          = 0x2A,
    /// Keyboard Tab
    KeyTab          = 0x2B,
    /// Keyboard Spacebar
    KeySpace        = 0x2C,
    /// **(2)** Keyboard - and (underscore)
    KeyMinus        = 0x2D,
    /// **(2)** Keyboard = and +
    KeyEqual        = 0x2E,
    /// **(2)** Keyboard [ and {
    KeyBracketLeft  = 0x2F,
    /// **(2)** Keyboard ] and }
    KeyBracketRight = 0x30,
    /// Keyboard \ and |
    KeyBackslash    = 0x31,
    /// **(5)** Keyboard Non-US # and ~
    KeyHashtag      = 0x32,
    /// **(2)** Keyboard ; and :
    KeySemicolon    = 0x33,
    /// **(2)** Keyboard ' and "
    KeyApostrophe   = 0x34,
    /// **(2)** Keyboard Grave Accent and Tilde
    KeyGraveAccent  = 0x35,
    /// **(2)** Keyboard , and <
    KeyComma        = 0x36,
    /// **(2)** Keyboard . and >
    KeyDot          = 0x37,
    /// **(2)** Keyboard / and ?
    KeySlash        = 0x38,
    /// **(6)** Keyboard Caps Lock
    KeyCapsLock     = 0x39,

    // Function keys
    /// Keyboard F1
    KeyF1  = 0x3A,
    /// Keyboard F2
    KeyF2  = 0x3B,
    /// Keyboard F3
    KeyF3  = 0x3C,
    /// Keyboard F4
    KeyF4  = 0x3D,
    /// Keyboard F5
    KeyF5  = 0x3E,
    /// Keyboard F6
    KeyF6  = 0x3F,
    /// Keyboard F7
    KeyF7  = 0x40,
    /// Keyboard F8
    KeyF8  = 0x41,
    /// Keyboard F9
    KeyF9  = 0x42,
    /// Keyboard F10
    KeyF10 = 0x43,
    /// Keyboard F11
    KeyF11 = 0x44,
    /// Keyboard F12
    KeyF12 = 0x45,

    // Editing
    /// **(7)** Keyboard PrintScreen
    KeyPrintScreen   = 0x46,
    /// **(6)** Keyboard Scroll Lock
    KeyScrollLock    = 0x47,
    /// **(7)** Keyboard Pause
    KeyPause         = 0x48,
    /// **(7)** Keyboard Insert
    KeyInsert        = 0x49,
    /// **(7)** Keyboard Home
    KeyHome          = 0x4A,
    /// **(7)** Keyboard PageUp
    KeyPageUp        = 0x4B,
    /// **(7, 8)** Keyboard Delete Forward
    KeyDeleteForward = 0x4C,
    /// **(7)** Keyboard End
    KeyEnd           = 0x4D,
    /// **(7)** Keyboard PageDown
    KeyPageDown      = 0x4E,

    // Arrow keys
    /// **(7)** Keyboard RightArrow
    KeyRightArrow = 0x4F,
    /// **(7)** Keyboard LeftArrow
    KeyLeftArrow  = 0x50,
    /// **(7)** Keyboard DownArrow
    KeyDownArrow  = 0x51,
    /// **(7)** Keyboard UpArrow
    KeyUpArrow    = 0x52,

    // Keypad
    /// **(6)** Keypad Num Lock and Clear
    KeypadNumLock = 0x53,
    /// **(7)** Keypad /
    KeypadSlash   = 0x54,
    /// Keypad *
    KeypadStar    = 0x55,
    /// Keypad -
    KeypadMinus   = 0x56,
    /// Keypad +
    KeypadPlus    = 0x57,
    /// **(3)** Keypad ENTER
    KeypadEnter   = 0x58,
    /// Keypad 1 and End
    Keypad1       = 0x59,
    /// Keypad 2 and Down Arrow
    Keypad2       = 0x5A,
    /// Keypad 3 and PageDown
    Keypad3       = 0x5B,
    /// Keypad 4 and Left Arrow
    Keypad4       = 0x5C,
    /// Keypad 5
    Keypad5       = 0x5D,
    /// Keypad 6 and Right Arrow
    Keypad6       = 0x5E,
    /// Keypad 7 and Home
    Keypad7       = 0x5F,
    /// Keypad 8 and Up Arrow
    Keypad8       = 0x60,
    /// Keypad 9 and PageUp
    Keypad9       = 0x61,
    /// Keypad 0 and Insert
    Keypad0       = 0x62,
    /// Keypad . and Delete
    KeypadDot     = 0x63,

    // Misc.
    /// **(9, 10)** Keyboard Non-US \ and |
    KeyBackSlashNonUS = 0x64,
    /// **(11)** Keyboard Application
    KeyApplication    = 0x65,
    /// **(1)** Keyboard Power
    KeyPower          = 0x66,

    // Keypad
    /// Keypad =
    KeypadEqual = 0x67,

    // Additional function keys
    /// Keyboard F13
    KeyF13 = 0x68,
    /// Keyboard F14
    KeyF14 = 0x69,
    /// Keyboard F15
    KeyF15 = 0x6A,
    /// Keyboard F16
    KeyF16 = 0x6B,
    /// Keyboard F17
    KeyF17 = 0x6C,
    /// Keyboard F18
    KeyF18 = 0x6D,
    /// Keyboard F19
    KeyF19 = 0x6E,
    /// Keyboard F20
    KeyF20 = 0x6F,
    /// Keyboard F21
    KeyF21 = 0x70,
    /// Keyboard F22
    KeyF22 = 0x71,
    /// Keyboard F23
    KeyF23 = 0x72,
    /// Keyboard F24
    KeyF24 = 0x73,

    // Misc.
    /// Keyboard Execute
    KeyExecute = 0x74,
    /// Keyboard Help
    KeyHelp    = 0x75,
    /// Keyboard Menu
    KeyMenu    = 0x76,
    /// Keyboard Select
    KeySelect  = 0x77,
    /// Keyboard Stop
    KeyStop    = 0x78,
    /// Keyboard Again
    KeyAgain   = 0x79,
    /// Keyboard Undo
    KeyUndo    = 0x7A,
    /// Keyboard Cut
    KeyCut     = 0x7B,
    /// Keyboard Copy
    KeyCopy    = 0x7C,
    /// Keyboard Paste
    KeyPaste   = 0x7D,
    /// Keyboard Find
    KeyFind    = 0x7E,

    // Audio
    /// Keyboard Mute
    KeyMute = 0x7F,
    /// Keyboard Volume Up
    KeyVolumeUp = 0x80,
    /// Keyboard Volume Down
    KeyVolumeDown = 0x81,

    // Locks
    /// **(12)** Keyboard Locking Caps Lock
    KeyLockCapsLock = 0x82,
    /// **(12)** Keyboard Locking Num Lock
    KeyLockNumLock = 0x83,
    /// **(12)** Keyboard Locking Scroll Lock
    KeyLockScrollLock = 0x84,

    // Keypad
    /// **(13)** Keypad Comma
    KeypadComma = 0x85,
    /// **(14)** Keypad Equal Sign
    KeypadEqualSign = 0x86,

    // International
    /// **(15, 16)** Keyboard International1
    KeyInternational1 = 0x87,
    /// **(17)** Keyboard International2
    KeyInternational2 = 0x88,
    /// **(18)** Keyboard International3
    KeyInternational3 = 0x89,
    /// **(19)** Keyboard International4
    KeyInternational4 = 0x8A,
    /// **(20)** Keyboard International5
    KeyInternational5 = 0x8B,
    /// **(21)** Keyboard International6
    KeyInternational6 = 0x8C,
    /// **(22)** Keyboard International7
    KeyInternational7 = 0x8D,
    /// **(23)** Keyboard International8
    KeyInternational8 = 0x8E,
    /// **(23)** Keyboard International9
    KeyInternational9 = 0x8F,

    // LANG
    /// **(24)** Keyboard LANG1
    KeyLang1 = 0x90,
    /// **(25)** Keyboard LANG2
    KeyLang2 = 0x91,
    /// **(26)** Keyboard LANG3
    KeyLang3 = 0x92,
    /// **(27)** Keyboard LANG4
    KeyLang4 = 0x93,
    /// **(28)** Keyboard LANG5
    KeyLang5 = 0x94,
    /// **(28)** Keyboard LANG6
    KeyLang6 = 0x95,
    /// **(28)** Keyboard LANG7
    KeyLang7 = 0x96,
    /// **(28)** Keyboard LANG8
    KeyLang8 = 0x97,
    /// **(28)** Keyboard LANG9
    KeyLang9 = 0x98,

    // Misc.
    /// **(30)** Keyboard Alternate Erase
    KeyAlternateErase = 0x99,
    /// **(7)** Keyboard SysReq/Attention
    KeySysReq         = 0x9A,
    /// Keyboard Cancel
    KeyCancel         = 0x9B,
    /// Keyboard Clear
    KeyClear          = 0x9C,
    /// Keyboard Prior
    KeyPrior          = 0x9D,
    /// Keyboard Return (Maybe you want [RegularKeyCode::KeyReturnEnter] instead)
    KeyReturn         = 0x9E,
    /// Keyboard Separator
    KeySeparator      = 0x9F,
    /// Keyboard Out
    KeyOut            = 0xA0,
    /// Keyboard Oper
    KeyOper           = 0xA1,
    /// Keyboard Clear/Again
    KeyClearAgain     = 0xA2,
    /// Keyboard CrSel/Props
    KeyCrSel          = 0xA3,
    /// Keyboard ExSel
    KeyExSel          = 0xA4,

    /* 0xA5 - 0xAF reserved */

    // Counting / Currency
    /// Keypad 00
    Keypad00           = 0xB0,
    /// Keypad 000
    Keypad000          = 0xB1,
    /// **(31)** Thousands Separator
    ThousandsSeparator = 0xB2,
    /// **(31)** Decimal Separator
    DecimalSeparator   = 0xB3,
    /// **(32)** Currency Unit
    CurrencyUnit       = 0xB4,
    /// **(32)** Currency Sub-unit
    CurrencySubUnit    = 0xB5,

    // Keypad Symbols
    /// Keypad (
    KeypadParenthesesLeft  = 0xB6,
    /// Keypad )
    KeypadParenthesesRight = 0xB7,
    /// Keypad {
    KeypadBracesLeft       = 0xB8,
    /// Keypad }
    KeypadBracesRight      = 0xB9,
    /// Keypad Tab
    KeypadTab              = 0xBA,
    /// Keypad Backspace
    KeypadBackspace        = 0xBB,
    /// Keypad A
    KeypadA                = 0xBC,
    /// Keypad B
    KeypadB                = 0xBD,
    /// Keypad C
    KeypadC                = 0xBE,
    /// Keypad D
    KeypadD                = 0xBF,
    /// Keypad E
    KeypadE                = 0xC0,
    /// Keypad F
    KeypadF                = 0xC1,
    /// Keypad XOR
    KeypadXOR              = 0xC2,
    /// Keypad ^
    KeypadCircumflex       = 0xC3,
    /// Keypad %
    KeypadPercentage       = 0xC4,
    /// Keypad <
    KeypadLess             = 0xC5,
    /// Keypad >
    KeypadMore             = 0xC6,
    /// Keypad &
    KeypadAnd              = 0xC7,
    /// Keypad &&
    KeypadAnd2             = 0xC8,
    /// Keypad |
    KeypadVerticalBar      = 0xC9,
    /// Keypad ||
    KeypadVerticalBar2     = 0xCA,
    /// Keypad :
    KeypadColon            = 0xCB,
    /// Keypad #
    KeypadHashtag          = 0xCC,
    /// Keypad Space
    KeypadSpace            = 0xCD,
    /// Keypad @
    KeypadAt               = 0xCE,
    /// Keypad !
    KeypadExclamationMark  = 0xCF,

    // Keypad math
    /// Keypad Memory Store
    KeypadMemoryStore    = 0xD0,
    /// Keypad Memory Recall
    KeypadMemoryRecall   = 0xD1,
    /// Keypad Memory Clear
    KeypadMemoryClear    = 0xD2,
    /// Keypad Memory Add
    KeypadMemoryAdd      = 0xD3,
    /// Keypad Memory Subtract
    KeypadMemorySubtract = 0xD4,
    /// Keypad Memory Multiply
    KeypadMemoryMultiply = 0xD5,
    /// Keypad Memory Divide
    KeypadMemoryDivide   = 0xD6,
    /// Keypad +/-
    KeypadPlusMinus      = 0xD7,
    /// Keypad Clear
    KeypadClear          = 0xD8,
    /// Keypad Clear Entry
    KeypadClearEntry     = 0xD9,
    /// Keypad Binary
    KeypadBinary         = 0xDA,
    /// Keypad Octal
    KeypadOctal          = 0xDB,
    /// Keypad Decimal
    KeypadDecimal        = 0xDC,
    /// Keypad Hexadecimal
    KeypadHexadecimal    = 0xDD,

    /* 0xDE - 0xDF reserved */

    // Modifiers
    /// Keyboard LeftControl
    KeyLeftControl  = 0xE0,
    /// Keyboard LeftShift
    KeyLeftShift    = 0xE1,
    /// Keyboard LeftAlt
    KeyLeftAlt      = 0xE2,
    /// **(11, 33)** Keyboard Left GUI
    KeyLeftGUI      = 0xE3,
    /// Keyboard RightControl
    KeyRightControl = 0xE4,
    /// Keyboard RightShift
    KeyRightShift   = 0xE5,
    /// Keyboard RightAlt
    KeyRightAlt     = 0xE6,
    /// **(11, 34)** Keyboard Right GUI
    KeyRightGUI     = 0xE7,

    /* 0xE8 - 0xFFFF reserved */
}

impl RegularKeycode {
    /// Returns whether this key is a modifier.
    #[inline]
    pub fn is_modifier(&self) -> bool {
        matches!(u16::from(*self), 0xE0..=0xE7)
    }

    /// Returns the reserved keycode regions `[(from, end including); 4]`
    pub const fn reserved() -> [(u16, u16); 4] {
        [(0x0, 0x0), (0xA5, 0xAF), (0xDE, 0xDF), (0xE8, 0xFFFF)]
    }

    /// Returns whether the given code is in the reserved space.
    pub const fn is_reserved(code: u16) -> bool {
        matches!(code, 0x0 | 0xA5..=0xAF | 0xDE..=0xDF | 0xE8..=0xFFFF)
    }

    /// Returns the number of reserved keycodes.
    pub const fn reserved_count() -> usize {
        // 0x0 + 1st range       + 2nd range         + rest
        // 1 + (0xAF - 0xA5 + 1) + (0xDF - 0xDE + 1) + (0xFFFF - 0xE8 + 1)
        65318
    }

    /// Returns the number of non-reserved keycodes.
    pub const fn unreserved_count() -> usize {
        // u16 can be safely represented as usize
        u16::MAX as usize - Self::reserved_count()
    }
}

#[derive(Copy, Clone, Debug)]
pub struct ReservedKeycodeError {
    code: u16,
}

impl fmt::Display for ReservedKeycodeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Reserved keycode: {}", self.code)
    }
}

impl TryFrom<u16> for RegularKeycode {
    type Error = ReservedKeycodeError;

    /// Tries to convert the given code to a regular keycode.
    ///
    /// If the code is inside the [reserved space](Self::is_reserved), an error will be returned.
    fn try_from(code: u16) -> Result<Self, Self::Error> {
        match Self::is_reserved(code) {
            // Reserved, non-existing codes
            true => Err(ReservedKeycodeError { code }),

            // SAFETY: - enum is #[repr(u16)]
            //         - every variant has a u16 specified
            //         - code is now legal
            false => Ok(unsafe { mem::transmute(code) }),
        }
    }
}

impl From<RegularKeycode> for u16 {
    #[inline]
    fn from(code: RegularKeycode) -> Self {
        code as u16
    }
}

macro_rules! impl_eq {
    ($left:ident, $right:ident) => {
        impl PartialEq<$left> for $right {
            fn eq(&self, other: &$left) -> bool {
                self == other
            }
        }

        impl PartialEq<$right> for $left {
            fn eq(&self, other: &$right) -> bool {
                self == other
            }
        }
    };
}

impl_eq!(RegularKeycode, u16);
impl_eq!(Keycode, u16);
impl_eq!(RegularKeycode, Keycode);

// Implement conversion from MDN code names to key codes.
// Taken from https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code/code_values
impl RegularKeycode {
    /// Tries to translate a codename into a regular keycode
    ///
    /// # Codenames
    /// See [here](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code/code_values)
    /// for possible values. Not every key translation has been implemented! In such a case
    /// [Option::None] will be returned.
    pub fn from_web_codename(codename: &str) -> Option<Self> {
        match codename {
            "Unidentified" | "" => None,
            "KeyA" => Some(Self::KeyA),
            "KeyB" => Some(Self::KeyB),
            "KeyC" => Some(Self::KeyC),
            "KeyD" => Some(Self::KeyD),
            "KeyE" => Some(Self::KeyE),
            "KeyF" => Some(Self::KeyF),
            "KeyG" => Some(Self::KeyG),
            "KeyH" => Some(Self::KeyH),
            "KeyI" => Some(Self::KeyI),
            "KeyJ" => Some(Self::KeyJ),
            "KeyK" => Some(Self::KeyK),
            "KeyL" => Some(Self::KeyL),
            "KeyM" => Some(Self::KeyM),
            "KeyN" => Some(Self::KeyN),
            "KeyO" => Some(Self::KeyO),
            "KeyP" => Some(Self::KeyP),
            "KeyQ" => Some(Self::KeyQ),
            "KeyR" => Some(Self::KeyR),
            "KeyS" => Some(Self::KeyS),
            "KeyT" => Some(Self::KeyT),
            "KeyU" => Some(Self::KeyU),
            "KeyV" => Some(Self::KeyV),
            "KeyW" => Some(Self::KeyW),
            "KeyX" => Some(Self::KeyX),
            "KeyY" => Some(Self::KeyY),
            "KeyZ" => Some(Self::KeyZ),
            "Digit0" => Some(Self::Key0),
            "Digit1" => Some(Self::Key1),
            "Digit2" => Some(Self::Key2),
            "Digit3" => Some(Self::Key3),
            "Digit4" => Some(Self::Key4),
            "Digit5" => Some(Self::Key5),
            "Digit6" => Some(Self::Key6),
            "Digit7" => Some(Self::Key7),
            "Digit8" => Some(Self::Key8),
            "Digit9" => Some(Self::Key9),
            "Grave" => Some(Self::KeyGraveAccent),
            "Subtract" => Some(Self::KeyMinus),
            "Equals" => Some(Self::KeyEqual),
            "BracketLeft" => Some(Self::KeyBracketLeft),
            "BracketRight" => Some(Self::KeyBracketRight),
            "Backslash" => Some(Self::KeyBackslash),
            "Semicolon" => Some(Self::KeySemicolon),
            "Apostrophe" => Some(Self::KeyApostrophe),
            "Comma" => Some(Self::KeyComma),
            "Period" => Some(Self::KeyDot),
            "Slash" => Some(Self::KeySlash),
            "Space" => Some(Self::KeySpace),
            "Backspace" => Some(Self::KeyDel),
            "Tab" => Some(Self::KeyTab),
            "ControlLeft" => Some(Self::KeyLeftControl),
            "ControlRight" => Some(Self::KeyRightControl),
            "AltLeft" => Some(Self::KeyLeftAlt),
            "AltRight" => Some(Self::KeyRightAlt),
            "Enter" => Some(Self::KeyReturnEnter),
            "Escape" => Some(Self::KeyEsc),
            "F1" => Some(Self::KeyF1),
            "F2" => Some(Self::KeyF2),
            "F3" => Some(Self::KeyF3),
            "F4" => Some(Self::KeyF4),
            "F5" => Some(Self::KeyF5),
            "F6" => Some(Self::KeyF6),
            "F7" => Some(Self::KeyF7),
            "F8" => Some(Self::KeyF8),
            "F9" => Some(Self::KeyF9),
            "F10" => Some(Self::KeyF10),
            "F11" => Some(Self::KeyF11),
            "F12" => Some(Self::KeyF12),
            "F13" => Some(Self::KeyF13),
            "F14" => Some(Self::KeyF14),
            "F15" => Some(Self::KeyF15),
            "F16" => Some(Self::KeyF16),
            "F17" => Some(Self::KeyF17),
            "F18" => Some(Self::KeyF18),
            "F19" => Some(Self::KeyF19),
            "F20" => Some(Self::KeyF20),
            "F21" => Some(Self::KeyF21),
            "F22" => Some(Self::KeyF22),
            "F23" => Some(Self::KeyF23),
            "F24" => Some(Self::KeyF24),
            "MetaLeft" | "OSLeft" => Some(Self::KeyLeftGUI),
            "MetaRight" | "OSRight" => Some(Self::KeyRightGUI),
            "ArrowUp" => Some(Self::KeyUpArrow),
            "PageUp" => Some(Self::KeyPageUp),
            "ArrowLeft" => Some(Self::KeyLeftArrow),
            "ArrowRight" => Some(Self::KeyRightArrow),
            "End" => Some(Self::KeyEnd),
            "ArrowDown" => Some(Self::KeyDownArrow),
            "PageDown" => Some(Self::KeyPageDown),
            "Delete" => Some(Self::KeyDeleteForward),
            "ShiftLeft" => Some(Self::KeyLeftShift),
            "ShiftRight" => Some(Self::KeyRightShift),
            _ => None,
        }
    }
}

bitflags::bitflags! {
    /// Key modifiers constants for evaluating pressed buttons.
    ///
    /// # Summary
    /// To simplify look-ups and increased performance we do not distinct between
    /// - `Left Control` and `Right Control` => `Control`
    /// - `Left Shift` and `Right Shift` => `Shift`
    /// - `Left GUI` and `Right GUI` => `GUI`
    ///
    /// But we do distinct between
    /// - `Alt`
    /// - `AltGr`
    pub struct KeyModifier: u8 {
        const CONTROL = 1 << 0;
        const SHIFT = 1 << 1;
        const GUI = 1 << 2;
        const ALT = 1 << 3;
        const ALT_GR = 1 << 4;
    }
}

impl KeyModifier {
    /// Returns whether the [KeyModifier::CONTROL] bit is set.
    #[inline]
    pub fn has_ctrl(&self) -> bool {
        self.contains(KeyModifier::CONTROL)
    }

    /// Returns whether the [KeyModifier::SHIFT] bit is set.
    #[inline]
    pub fn has_shift(&self) -> bool {
        self.contains(KeyModifier::SHIFT)
    }

    /// Returns whether the [KeyModifier::GUI] bit is set.
    #[inline]
    pub fn has_gui(&self) -> bool {
        self.contains(KeyModifier::GUI)
    }

    /// Returns whether the [KeyModifier::ALT] bit is set.
    #[inline]
    pub fn has_alt(&self) -> bool {
        self.contains(KeyModifier::ALT)
    }

    /// Returns whether the [KeyModifier::ALT_GR] bit is set.
    #[inline]
    pub fn has_altgr(&self) -> bool {
        self.contains(KeyModifier::ALT_GR)
    }
}

#[derive(Copy, Clone, Debug)]
pub struct IntoModifierError<T> {
    value: T,
}

impl<T: Debug> Display for IntoModifierError<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "cannot translate '{:?}' into KeyModifier", self.value)
    }
}

impl<'a> TryFrom<&'a str> for KeyModifier {
    type Error = IntoModifierError<&'a str>;

    /// Tries to convert the given string into a key modifier.
    ///
    /// # Valid values
    /// - `Control` | `Ctrl` => [Control](Self::CONTROL)
    /// - `Shift` => [Shift](Self::SHIFT)
    /// - `GUI` => [GUI](Self::GUI)
    /// - `Alt"` => [Alt](Self::ALT)
    /// - `AltGr` => [AltGr](Self::ALT_GR)
    ///
    /// # Errors
    /// This function returns an error if the value does not correspond to any of the above strings.
    fn try_from(value: &'a str) -> Result<Self, Self::Error> {
        match value {
            "Control" | "Ctrl" => Ok(Self::CONTROL),
            "Shift" => Ok(Self::SHIFT),
            "GUI" => Ok(Self::GUI),
            "Alt" => Ok(Self::ALT),
            "AltGr" => Ok(Self::ALT_GR),
            _ => Err(IntoModifierError { value }),
        }
    }
}

impl TryFrom<Keycode> for KeyModifier {
    type Error = IntoModifierError<Keycode>;

    /// Tries to convert the given keycode into a key modifier.
    ///
    /// # Valid values
    /// - [`Left Control`](RegularKeyCode::KeyLeftControl) | [`Right Control`](RegularKeyCode::KeyRightControl) => [Control](Self::CONTROL)
    /// - [`Left Shift`](RegularKeyCode::KeyLeftShift) | [`Right Shift`](RegularKeyCode::KeyRightShift) => [Shift](Self::SHIFT)
    /// - [`Left GUI`](RegularKeyCode::KeyLeftGUI) | [`Right GUI`](RegularKeyCode::KeyRightGUI) => [GUI](Self::GUI)
    /// - [`Left Alt`](RegularKeyCode::KeyLeftAlt) => [Alt](Self::ALT)
    /// - [`Right Alt / AltGr`](RegularKeyCode::KeyLeftAlt) => [AltGr](Self::ALT_GR)
    ///
    /// # Errors
    /// This function returns an error if the value does not correspond to any of the above keycodes.
    fn try_from(value: Keycode) -> Result<Self, Self::Error> {
        match value {
            Keycode::Regular(RegularKeycode::KeyLeftControl | RegularKeycode::KeyRightControl) => {
                Ok(Self::CONTROL)
            }
            Keycode::Regular(RegularKeycode::KeyLeftShift | RegularKeycode::KeyRightShift) => {
                Ok(Self::SHIFT)
            }
            Keycode::Regular(RegularKeycode::KeyLeftGUI | RegularKeycode::KeyRightGUI) => {
                Ok(Self::GUI)
            }
            Keycode::Regular(RegularKeycode::KeyLeftAlt) => Ok(Self::ALT),
            Keycode::Regular(RegularKeycode::KeyRightAlt) => Ok(Self::ALT_GR),
            _ => Err(IntoModifierError { value }),
        }
    }
}
