use crate::buttoncode::ButtonCode;
use crate::keycode::Keycode;

// Synced from https://gitlab.redox-os.org/redox-os/orbclient/-/blob/8ef4433da9871d6f6e8c11d0b863f737144d19f1/src/event.rs#L4-19
pub const EVENT_KEY: i64 = 1;
pub const EVENT_MOUSE: i64 = 2;
pub const EVENT_BUTTON: i64 = 3;
pub const EVENT_MOUSE_RELATIVE: i64 = 11;

/// A generic input event to send via pipes.
#[derive(Copy, Clone, Debug)]
#[repr(packed)]
pub struct InputEvent {
    /// Event type
    pub event_type: i64,
    /// Either `i64` data or `bool` depending on [Self::event_type]
    pub a: i64,
    /// Either `i64` data or `bool` depending on [Self::event_type]
    pub b: i64,
}

impl InputEvent {
    pub fn new(event_type: i64, a: i64, b: i64) -> Self {
        Self { event_type, a, b }
    }
}

impl core::ops::Deref for InputEvent {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        let len = core::mem::size_of::<InputEvent>();

        // SAFETY: - an input event consists of native `i64`s and is contiguous memory
        //         - only the memory region of the struct itself is taken
        //         - the lifetime of returned slice is inferred from its usage
        unsafe { core::slice::from_raw_parts(self as *const InputEvent as *const u8, len) as &[u8] }
    }
}

impl From<KeyEvent> for InputEvent {
    fn from(key_event: KeyEvent) -> Self {
        match key_event {
            KeyEvent::KeyDown(keycode) => Self::new(EVENT_KEY, keycode.into(), true.into()),
            KeyEvent::KeyUp(keycode) => Self::new(EVENT_KEY, keycode.into(), false.into()),
        }
    }
}

impl From<MouseEvent> for InputEvent {
    fn from(mouse_event: MouseEvent) -> Self {
        match mouse_event {
            MouseEvent::Move(x, y) => Self::new(EVENT_MOUSE, x, y),
            MouseEvent::MoveRel(dx, dy) => Self::new(EVENT_MOUSE_RELATIVE, dx, dy),
        }
    }
}

impl From<ButtonEvent> for InputEvent {
    fn from(button_event: ButtonEvent) -> Self {
        match button_event {
            ButtonEvent::ButtonDown(button) => Self::new(EVENT_BUTTON, button.into(), true.into()),
            ButtonEvent::ButtonUp(button) => Self::new(EVENT_BUTTON, button.into(), false.into()),
        }
    }
}

/// Describes possible key input events.
///
/// To send via pipes, convert to an [InputEvent] and deref.
#[derive(Copy, Clone, Debug)]
pub enum KeyEvent {
    /// The key press.
    KeyDown(Keycode),
    /// The key release.
    KeyUp(Keycode),
}

/// Describes possible mouse input events.
///
/// To send via pipes, convert to an [InputEvent] and deref.
#[derive(Copy, Clone, Debug)]
pub enum MouseEvent {
    /// The mouse movement in absolute coordinates `x` and `y`.
    Move(i64, i64),
    /// The mouse movement in relative coordinates `dx` and `dy`.
    MoveRel(i64, i64),
}

/// Describes possible button input events.
///
/// To send via pipes, convert to an [InputEvent] and deref.
#[derive(Copy, Clone, Debug)]
pub enum ButtonEvent {
    /// The button press.
    ButtonDown(ButtonCode),
    /// The button release.
    ButtonUp(ButtonCode),
}
