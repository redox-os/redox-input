pub mod buttoncode;
pub mod event;
pub mod keycode;

pub use buttoncode::*;
pub use event::*;
pub use keycode::*;

mod macro_impl {
    use crate::*;
    use core::convert::TryFrom;
    use core::num::ParseIntError;

    /// Converts a `u16` string (with/without `(0x|0X)` prefix) to the `$target.
    macro_rules! from_u16_str {
        ($target:ident) => {
            impl TryFrom<&str> for $target {
                type Error = ParseIntError;

                /// Tries parsing [Self] from a stringified u16 value.
                ///
                /// # Summary
                /// This conversion assumes the value to either be
                /// - decimal, or
                /// - hexadecimal with or without the `(0x|0X)` prefix
                fn try_from(value: &str) -> Result<Self, Self::Error> {
                    let code = match value.parse::<u16>() {
                        Ok(c) => c,
                        Err(_) => u16::from_str_radix(
                            value.trim_start_matches("0x").trim_start_matches("0X"),
                            16,
                        )?,
                    };

                    Ok(Self::from(code))
                }
            }
        };
    }

    from_u16_str!(ButtonCode);
    from_u16_str!(Keycode);

    /// Converts the `$origin` to the `$target`s.
    ///
    /// # Summary
    /// Assumes that `Into<u16>` is implemented for `$origin` as this one original conversion is used
    /// for the other integers.
    macro_rules! to_integers {
        ($origin:ident => $($target:ident),+) => {
            $(
                impl From<$origin> for $target {
                    fn from(value: $origin) -> Self {
                        Self::from(Into::<u16>::into(value))
                    }
                }
            )+
        };
    }

    // isize does not cover u16 on platforms with 16bit pointer size
    to_integers!(ButtonCode => u32, u64, usize, i32, i64);
    to_integers!(Keycode => u32, u64, usize, i32, i64);
    to_integers!(RegularKeycode => u32, u64, usize, i32, i64);
}
