use core::convert::TryFrom;
use core::num::NonZeroU16;

/// USB HID button code constants.
///
/// # Summary
/// This list is modeled after the button codes defined in
/// <https://usb.org/sites/default/files/hut1_22.pdf#page=103>.
/// All other codes can be
#[repr(u16)]
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum ButtonCode {
    /// No Button Pressed
    None,
    Code(NonZeroU16),
}

impl ButtonCode {
    /// Creates the primary (logical left) button
    #[inline]
    pub fn primary() -> Self {
        Self::from(1)
    }

    /// Creates the secondary (logical right) button
    #[inline]
    pub fn secondary() -> Self {
        Self::from(2)
    }

    /// Creates the tertiary (logical middle) button
    #[inline]
    pub fn tertiary() -> Self {
        Self::from(3)
    }

    /// Returns whether this button is primary (logical left)
    #[inline]
    pub fn is_primary(&self) -> bool {
        *self == Self::from(1)
    }

    /// Returns whether this button is secondary (logical right)
    #[inline]
    pub fn is_secondary(&self) -> bool {
        *self == Self::from(2)
    }

    /// Returns whether this button is tertiary (logical middle)
    #[inline]
    pub fn is_tertiary(&self) -> bool {
        *self == Self::from(3)
    }
}

impl From<u16> for ButtonCode {
    #[inline]
    fn from(code: u16) -> Self {
        match NonZeroU16::try_from(code) {
            Err(_) => Self::None,
            Ok(c) => Self::Code(c),
        }
    }
}

impl From<ButtonCode> for u16 {
    #[inline]
    fn from(button: ButtonCode) -> Self {
        match button {
            ButtonCode::None => 0,
            ButtonCode::Code(code) => u16::from(code),
        }
    }
}
